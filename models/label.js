const mongoose = require("mongoose");

const LabelSchema = mongoose.Schema({
    nombre: {
        type:String,
        required: true
    },
    numero:{
        type:Number,
        required: true,
    },
    creador: {
        type:String,
        required: false
    },

});

module.exports= mongoose.model('Label',LabelSchema)