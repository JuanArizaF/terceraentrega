const mongoose = require("mongoose");

const ActualSchema = mongoose.Schema({
    nombre: {
        type:String,
        required: true
    }
});

module.exports= mongoose.model('Actual',ActualSchema)