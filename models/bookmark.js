const mongoose = require("mongoose");

const BookmarkSchema = mongoose.Schema({
    nombre: {
        type:String,
        required: true
    },
    url: {
        type:String,
        required: true
    },
    comentario:{
        type:String,
        required: false
    },
    fecha: {
        type: Date,
        default:Date.now,
        required: true
    },
    labels:{
        type:String,
        required: false
    },
    login: {
        type:String,
        required: false
    },
    password: {
        type:String,
        required: false
    },
    creador: {
        type:String,
        required: false
    },
});

module.exports= mongoose.model('Bookmark',BookmarkSchema)