const express = require("express");
const mongoose = require("mongoose");
const bookmarkRoute =require('./routes/bookmarks');
const usuarioRoute =require('./routes/usuarios');
const labelRoute =require('./routes/labels');
const actualRoute =require('./routes/actuals');
const bodyParser = require("body-parser");
const cors = require("cors");

require("dotenv/config");

const app=express();
app.use(cors())
app.use(bodyParser.json())

app.use('/bookmarks', bookmarkRoute)
app.use('/usuarios', usuarioRoute)
app.use('/labels', labelRoute)
app.use('/actuals', actualRoute)

app.get("/",(req, res) =>{
    res.send("Hola Express");
});

	
mongoose.connect(process.env.CONEXION_DB,
    
    { useUnifiedTopology: true, useNewUrlParser: true },
    () => {
    console.log("Conectado a la base de datos...");
    }
  );

app.listen(3000);