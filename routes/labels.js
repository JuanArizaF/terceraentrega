const express = require('express');
const Label = require('../models/label');
const router = express.Router();


//Traer todos los labels
router.get("/:creador", async(req, res) =>{
    let creador= req.params.creador;
    console.log("el indentificador es" + creador);
    // res.send("Hola mi so tareas");
    try {
        const labels= await Label.find().find({creador:creador});;
        res.json(labels)
        console.log(labels);
    } catch (error) {
        res.status(500).send(error)
    }
    
    
    res.send("obteniedo tarea")
});

//crear un labels
router.post("/",async(req, res) =>{
    const number1= await Label.countDocuments();
    let number2=0
    // console.log(typeof number2[0].numero);
    // console.log(typeof number1);
    let number = number1
    if (number != 0) {
        //console.log("llego al post de labels :"+number2[0].numero);
        try {
            number2= await Label.find().sort({numero:-1}).limit(1)
            number = number2[0].numero +1
            // console.log("Entro");
        } catch (error) {
            res.status(500).send(error);
        }
    }
    try{
        console.log("este es el numero : "+ number);
        const nuevaLabel = new Label({
            nombre: req.body.nombre,
            creador: req.body.creador,
            numero: number ,
        })
    
        let resultado= await nuevaLabel.save();
    
        // res.send("Creando una tarea...");
        res.json(resultado);
    }catch (error){
        res.status(500).send(error);

    }
    
});

//eliminar un label
router.delete('/:numero',async (req,res)=>{
    let numero= req.params.numero;
    console.log("el indentificador es" + numero);
    try {
        const resultado= await Label.remove({numero:numero})
        res.json(resultado)
    } catch (error) {
        res.status(500).send(error)
    }
    res.send("obteniedo tarea")
})

//editar un label
router.patch("/:numero",async (req,res)=>{
    let numero =req.params.numero
    try {
       const resultados =await Label.updateOne(
           {numero:numero},
           {
               $set:req.body,
           }
       )
       return res.json(resultados)
      //  res.json({message:'el numero  es '+ numero})
    } catch (error) {
        return res.status(500).send(error)
    }
})

module.exports = router;