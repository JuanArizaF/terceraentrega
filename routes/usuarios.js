const express = require('express');
const Usuario = require('../models/usuario');
const router = express.Router();


//Traer todos los usuario
router.get("/all", async(req, res) =>{
    // res.send("Hola mi so tareas");
    try {
        const usuarios= await Usuario.find();
        res.json(usuarios)
        console.log(usuarios);
    } catch (error) {
        res.status(500).send(error)
    }
});

//crear un usuario
router.post("/login",async(req, res) =>{
    console.log("llego al post de tareas");
    try{
        const nuevaUsuario = new Usuario({
            username: req.body.username,
            password: req.body.password,
            nombre: req.body.nombre,
            email: req.body.email,
        })
    
        let resultado= await nuevaUsuario.save();
    
        // res.send("Creando una tarea...");
        res.json(resultado);
    }catch (error){
        res.status(500).send(error);

    }
    
});



module.exports = router;