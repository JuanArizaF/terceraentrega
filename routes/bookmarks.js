const express = require('express');
const Bookmark = require('../models/bookmark');
const router = express.Router();


//Traer todos los bookmarks
router.get("/:creador", async(req, res) =>{
    // res.send("Hola mi so tareas");
    let creador= req.params.creador;
    console.log("el indentificador es" + creador);
    try {
        const bookmark =await Bookmark.find({creador:creador});
        res.json(bookmark)
    } catch (error) {
        res.status(500).send(error)
    }
    res.send("obteniedo tarea")
});
//Traer un bookmark 
router.get('/name/:nombre',async (req,res)=>{
    let nombre= req.params.nombre;
    console.log("el indentificador es" + nombre);
    try {
        const bookmark =await Bookmark.find({nombre:nombre});
        res.json(bookmark)
    } catch (error) {
        res.status(500).send(error)
    }
    res.send("obteniedo tarea")
})
//eliminar un bookmark
router.delete('/:nombre',async (req,res)=>{
    let nombre= req.params.nombre;
    console.log("el indentificador es" + nombre);
    try {
        const resultado= await Bookmark.remove({nombre:nombre})
        res.json(resultado)
    } catch (error) {
        res.status(500).send(error)
    }
    res.send("obteniedo tarea")
})
//editar un bookmark
router.patch("/:nombre",async (req,res)=>{
    let nombre =req.params.nombre
    try {
       const resultados =await Bookmark.updateOne(
           {nombre:nombre},
           {
               $set:req.body,
           }
       )
       return res.json(resultados)
      //  res.json({message:'el nombre recivnombreo es '+ nombre})
    } catch (error) {
        return res.status(500).send(error)
    }
})
//crear un bookmark
router.post("/",async(req, res) =>{
    console.log("llego al post de tareas");
    
    try{
        const nuevaBookmark = new Bookmark({
            nombre: req.body.nombre,
            comentario: req.body.comentario,
            url: req.body.url,
            labels: req.body.labels,
            login: req.body.login,
            password: req.body.password,
            creador: req.body.creador,

        })
    
        let resultado= await nuevaBookmark.save();
    
        // res.send("Creando una tarea...");
        res.json(resultado);
    }catch (error){
        res.status(500).send(error);

    }
    
});
//Traer por labels
router.get('/label/:labels',async (req,res)=>{
    let labels= req.params.labels;
    console.log("el indentificador es" + labels);
    try {
        const bookmark =await Bookmark.find({labels:labels});
        res.json(bookmark)
    } catch (error) {
        res.status(500).send(error)
    }
    res.send("obteniedo tarea")
})


module.exports = router;
