# BACKEND DE LA TERCERA ENTREGA PARA LA ASIGNATURA DE APLICACIONES DE SOFTWARE EN TELECOMUNICACIONES

Usando:
- NodeJS
- Express
- MongoDB
- Mongoose

## Instalación
Para poder instalar este proyecto ejecute:
''' bash
npm install
'''

## Ejecución 

Para poder ejecutarlo:

''' bash
npm start
'''

# Conexión a base de datos

Cree el archivo '.env', agregue la cadena de conexión con la variable CONEXION_DB y conéctelo a su base de datos.

Created by:

Juan Felipe Ariza Flechas
Laura Paola Goyeneche Gómez